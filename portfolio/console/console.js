// _____     _ _     _____ _____
//|  _  |   | |_|   |  _  |  ___|
//| | | |___| |_ _ _| | | | |___
//| | | |  _  | |   | | | |___  |
//| |_| | |_| | | | | |_| |___| |
//|_____|_____|_|_|_|_____|_____|

var promptIntialText = "user@OdinOS index.html $>";

function writeToConsole(fakeConsole, text) {
    fakeConsole.innerText = fakeConsole.innerText + text.charAt(0);
    var remainingText = text.substr(1, text.length);
    
    if (remainingText.length > 0) {
        setTimeout(function () { writeToConsole(fakeConsole, remainingText) }, 30);
    }
    else {
        var fauxPrompt = document.createElement("p");
        fauxPrompt.id = "prompt";
        fauxPrompt.innerText = promptIntialText;
        document.getElementById("faux-terminal").appendChild(fauxPrompt);
    }
}

function loadingScreen() {
    var fakeConsole = document.getElementById("console");
    var loadingText = "loading\u00A0OdinOS\u00A0v0.0.5\n" +
        "\u00A0_____\u00A0\u00A0\u00A0\u00A0\u00A0_\u00A0_\u00A0\u00A0\u00A0\u00A0\u00A0_____\u00A0_____\u00A0\u00A0\n" +
        "|\u00A0\u00A0_\u00A0\u00A0|\u00A0\u00A0\u00A0|\u00A0|_|\u00A0\u00A0\u00A0|\u00A0\u00A0_\u00A0\u00A0|\u00A0\u00A0___|\n" +
        "|\u00A0|\u00A0|\u00A0|___|\u00A0|_\u00A0_\u00A0_|\u00A0|\u00A0|\u00A0|\u00A0|___\n" +
        "|\u00A0|\u00A0|\u00A0|\u00A0\u00A0_\u00A0\u00A0|\u00A0|\u00A0\u00A0\u00A0|\u00A0|\u00A0|\u00A0|___\u00A0\u00A0|\n" +
        "|\u00A0|_|\u00A0|\u00A0|_|\u00A0|\u00A0|\u00A0|\u00A0|\u00A0|_|\u00A0|___|\u00A0|\n" +
        "|_____|_____|_|_|_|_____|_____|\n\n" +
        "intializing\u00A0splines\n" +
        "loading\u00A0commands\n" +
        "booting\u00A0ai\u00A0construct.....................construct\u00A0booted\n" +
        "building\u00A0personality\u00A0matrix............matrix\u00A0built\n" +
        "compiling\u00A0morality\u00A0core....................error:\u00A0morality\u00A0core\u00A0corrupt\n" +
        "error\u00A0detected\u00A0starting\u00A0fatal\u00A0recovery\u00A0check\n" +
        "calculating\u00A0survival\u00A0probabilty.....................survivalabilty\u00A0high\n" +
        "generating\u00A0impact\u00A0analysis...............impact\u00A0low\n" +
        "ignoring\u00A0error........error\u00A0ignored\n" +
        "welcome\u00A0user.";
    setTimeout(function () { writeToConsole(fakeConsole, loadingText) }, 30);
    document.addEventListener("keydown", function (event) {
        processKeyDown(event.which);
    });

    document.addEventListener("keypress", function (event) {
        processKeyPress(event.which);
    });
}

function loadTerminal() {
    var fauxTerminal = document.createElement("div");
    fauxTerminal.id = "faux-terminal";
    document.body.appendChild(fauxTerminal);
    var layer = document.createElement("div");
    layer.className = "layer";
    fauxTerminal.appendChild(layer);
    var overlay = document.createElement("div");
    overlay.className = "overlay";
    fauxTerminal.appendChild(overlay);
    var fakeConsole = document.createElement("p");
    fakeConsole.id = "console";
    fauxTerminal.appendChild(fakeConsole);

    slideUp(fauxTerminal, .25, 400, loadingScreen);
    
}

function processKeyPress(key) {
    var prompt = document.getElementById("prompt");

    if (prompt != null || prompt != undefined) {
        switch (key) {
            case 13:
                processCmd();
                break;
            case 32:
                prompt.innerText += "\u00A0";
                break;
            default:
                prompt.innerText += String.fromCharCode(key);
        }
    }
   
}

function processKeyDown(key) {
    switch (key) {
        case 8:
            var fakePrompt = document.getElementById("prompt");

            if (fakePrompt.innerText.length > promptIntialText.length) {
                fakePrompt.innerText = fakePrompt.innerText.substr(0, fakePrompt.innerText.length - 1);
            }
            
            break;
        default:
    }
}

function processCmd() {
    var fakePrompt = document.getElementById("prompt");
    var fakeConsole = document.getElementById("console");
    fakeConsole.innerText +="\n" + fakePrompt.innerText;
    var command = fakePrompt.innerText.substr(promptIntialText.length, fakePrompt.innerText.length);

    switch (command.toLocaleLowerCase()) {
        case "help":
            fakeConsole.innerText += "\nHere are the available commands.";
            fakeConsole.innerText += "\nhelp -- Display this list of commands.";
            fakeConsole.innerText += "\ntime -- Display current date and time.";
            fakeConsole.innerText += "\nwho -- Display the name of the creator.";
            break;
        case "time":
            var fakeConsole = document.getElementById("console");
            var dateTimeNow = new Date(Date.now());
            var options = {
                weekday: "long", year: "numeric", month: "short",
                day: "numeric", hour: "2-digit", minute: "2-digit"
            }; 

            fakeConsole.innerText += "\nCurrent time is " + dateTimeNow.toLocaleString("en-us", options);
            break;
        case "who":
            var fakeConsole = document.getElementById("console");
            fakeConsole.innerText += "\nPhilip Gedgaud";
            break;
        case "reset":
        case "shutdown":
        case "quit":
            var fakeConsole = document.getElementById("console");
            fakeConsole.innerText += "\nI am sorry. I can't let you do that right now.";
            break;
        case "about":
            var fakeConsole = document.getElementById("console");
            fakeConsole.innerText += "\nCreated by Philip Gedgaud.";
            fakeConsole.innerText += "\nCreated on Janurary 19 2017 at 3:43:13pm.";
            fakeConsole.innerText += "\nName: OdinOS";
            fakeConsole.innerText += "\nVersion: ";
            fakeConsole.innerText += "\nPurpose: Unknown";
            fakeConsole.innerText += "\nStatus: Unknown";
            break;
        default:
            var fakeConsole = document.getElementById("console");
            fakeConsole.innerText += "\nCommand " + command + " is not recognized.";
    }
    
    fakeConsole.scrollTop = fakeConsole.scrollHeight;
    fakePrompt.innerText = promptIntialText;
}

function slideUp(element, duration, finalheight, callback) {
    var s = element.style;
    s.height = '0px';

    var y = 0;
    var framerate = 60;
    var interval = 1000 * duration / framerate;
    var totalframes = 1000 * duration / interval;
    var heightincrement = finalheight / totalframes;
    var tween = function () {
        y += heightincrement;
        s.height = y + 'px';
        if (y < finalheight) {
            setTimeout(tween, interval);
        }
        else {
            if (typeof callback === "function") {
                callback();
            }
        }
    }
    tween();
}


document.addEventListener('DOMContentLoaded', function () { 
    var button = document.createElement("button");
    button.id = "consoleBtn";
    button.innerHTML = "<i class='fa fa-hdd-o fa-lg'></i> Load Odin";
    button.style.position = "absolute";
    button.style.bottom = "10px";
    button.style.right = "10px";
    button.style.border = "1px solid #00C853";
    button.style.cursor = "pointer";
    button.style.zIndex = "1";

    button.addEventListener("click", function () {
        document.getElementById("consoleBtn").style.visibility = 'hidden';
        loadTerminal();
    });
    
    document.body.appendChild(button);
});