class GameOfLifeDisplay {
    constructor() { 
        this.cellSize = 12;
        this.run = false;
        this.drawToCanvas = true;
        this.drawToDiv = false;
        this.drawToConsole = false;
        this.isRunning = false;
        this.gridPadding = 10;
    }

    setupGame() {
        this.createPopup();
        this.createCanvas();
        document.getElementById("run").addEventListener("click", () => this.pausePlay());
        document.getElementById("step").addEventListener("click", () => this.stepButtonClick());
        document.getElementById("newGame").addEventListener("click", () => this.generateNewGame());
        document.getElementById("clear").addEventListener("click", () => this.clearCanvas());
        this.canvas = document.getElementById("gameOfLifeCanvas");
        this.canvasContext = this.canvas.getContext("2d");
        this.gridHeight = Math.floor(this.canvas.height/this.cellSize) - 1;
        this.gridWidth =  Math.floor(this.canvas.width/this.cellSize) - 1;
        this.canvas.addEventListener('click', (evt) => this.canvasClick(evt), false);
        this.game = new GameOfLife(this.gridWidth, this.gridHeight);
        this.game.clearGrid();
        this.game.seedGrid();
        this.drawCycle();
    }
    
    createPopup() {
        var popup = document.createElement("div");
        popup.setAttribute("id", "gameOfLifePopup");
        var closeButton = document.createElement("button");
        closeButton.setAttribute("id", "gameOfLifeCloseBtn")
        closeButton.innerHTML = '<i class="fa fa-window-close-o fa-lg" aria-hidden="true"></i>';
        closeButton.addEventListener("click", () => this.closePopup());
        popup.appendChild(closeButton);
        document.body.appendChild(popup);
        var gameDiv = document.createElement("div");
        gameDiv.setAttribute("id", "gameOfLifeBoard");
        popup.appendChild(gameDiv);
        var gameControls = document.createElement("div");
        gameControls.setAttribute("id", "gameControls");
        gameControls.innerHTML = `<button class="gameOfLifeBtn" type="button" id="run" >
            <i class="fa fa-play fa-lg" aria-hidden="true"></i>
            <i class="fa fa-pause fa-lg" aria-hidden="true"></i>
        </button>
        <button class="gameOfLifeBtn" type="button" id="step" >
            <i class="fa fa-step-forward fa-lg" aria-hidden="true"></i>
        </button>
        <button class="gameOfLifeBtn" type="button" id="newGame" >New Game</button>
        <button class="gameOfLifeBtn" type="button" id="clear" >Clear Game</button>
        <p>Click on a cell to populate it.</p>`;
        popup.appendChild(gameControls);
    }
 
    closePopup() {
        //clear interval and remove event listeners
        var parent = document.getElementById("gameOfLifePopup").parentElement;
        parent.removeChild(document.getElementById("gameOfLifePopup"));
    }

    createCanvas() {
        this.gameBoard = document.getElementById("gameOfLifeBoard");
        this.gameBoard.style.width ='100%';
        this.gameBoard.style.height ='85%';
        this.boardWidth = Math.floor((this.gameBoard.clientWidth - this.gridPadding * 4) / this.cellSize) * this.cellSize;
        this.boardHeight = Math.floor((this.gameBoard.clientHeight - this.gridPadding * 4) / this.cellSize) * this.cellSize; 
        this.canvasWidth = this.boardWidth + this.gridPadding * 2 + 1;
        this.canvasHeight = this.boardHeight + this.gridPadding * 2 + 1;
        var canvas = document.createElement("canvas");
        canvas.setAttribute("width", this.canvasWidth);
        canvas.setAttribute("height", this.canvasHeight);
        canvas.setAttribute("id", "gameOfLifeCanvas");
        //canvas.innerHTML("Your browser does not support the HTML5 canvas tag.");
        this.gameBoard.appendChild(canvas);
    }

    generateNewGame(){
        this.clearCanvas();   
        this.game.generateRandomBoard();
        this.drawCycle();
    }

    clearCanvas(){
        this.game.clearGrid();
        this.drawCycle();
    }

    canvasClick(evt) {
        var mousePos = this.getMousePos(this.canvas, evt);
        this.game.setCellAlive(mousePos.xPos, mousePos.yPos);
        this.drawCycle();
    }

    pausePlay() {
        if(this.isRunning === true) {
            this.pause();
            this.isRunning = false;
        }
        else {
            this.runGame();
            this.isRunning = true;
        }
    }

    runGame() {
        this.interval = setInterval(() => this.step(), 1000);
    }

    pause() {
        clearInterval(this.interval);
    }

    drawCycle() {
        if (this.drawToConsole) {
           this.drawToConsole();
        } else if (this.drawToCanvas) {
           this.draw2Canvas();
        }
    }

    drawToConsole() {
        var outputStr = '';
        for (var x = 0; x < this.theGrid.length; x++) {
            outputStr = '';
            
            for (var y = 0; y < this.theGrid[x].length; y++) {
                if (this.theGrid[x][y] === 0) {
                    outputStr += ' ';
                } else {
                    outputStr += '#';
                }
            }
            
            console.log(outputStr);
        }
    }

    draw2Canvas() {
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawGrid();
        this.canvasContext.beginPath();
        this.canvasContext.fillStyle = "rgba(255,153,0,0.5)";
        this.canvasContext.shadowBlur = 10;
        this.canvasContext.shadowColor = 'rgba(255,153,0,0.5)';
        for (var x = 0; x < this.gridWidth; x++) {
            for (var y = 0; y < this.gridHeight; y++) { 
                if (this.game.isCellAlive(x, y)) {
                    this.canvasContext.fillRect(
                        (x * this.cellSize + this.gridPadding),
                        (y * this.cellSize + this.gridPadding), this.cellSize, this.cellSize);
                }
            }
        }
        
        this.canvasContext.closePath();
    }

    drawGrid() {
        this.canvasContext.strokeStyle = 'rgba(0,178,255,0.25)';
        this.canvasContext.shadowBlur = 10;
        this.canvasContext.shadowColor = 'rgba(0,178,255,0.5)';
        this.canvasContext.beginPath();

        for (var x = 0; x < this.gridWidth; x++) {
            this.canvasContext.beginPath();
            this.canvasContext.moveTo(0.5 + x * this.cellSize + this.gridPadding, this.gridPadding);
            this.canvasContext.lineTo(0.5 + x * this.cellSize + this.gridPadding, this.gridPadding + this.boardHeight);
            this.canvasContext.stroke();
            this.canvasContext.closePath();
        }

        for (var y = 0; y < this.gridHeight; y++) {
            this.canvasContext.beginPath();
            this.canvasContext.moveTo(this.gridPadding, 0.5 + y * this.cellSize + this.gridPadding);
            this.canvasContext.lineTo(this.boardWidth + this.gridPadding, 0.5 + y * this.cellSize + this.gridPadding);
            this.canvasContext.stroke();  
        }

        this.canvasContext.closePath();
    }

    step() {
        this.game.calculateNextCycle();
        this.drawCycle();
    }
    
    stepButtonClick(){
        if(this.isRunning === false) {
            this.step();    
        }
    }

    getMousePos(canvas, evt) {
        var rect = this.canvas.getBoundingClientRect();
        var x = evt.clientX - rect.left;
        var y = evt.clientY - rect.top;
        var width = canvas.width;
        var height = canvas.height;
        return { 
            xPos: Math.floor((x - this.gridPadding)/(width/this.gridWidth)), 
            yPos: Math.floor((y - this.gridPadding)/(height/this.gridHeight))
        };
    }
}

document.addEventListener("DOMContentLoaded", function(){
    document.getElementById("gameOfLifeStartButton").addEventListener("click", function(){
            var game = new GameOfLifeDisplay();
            game.setupGame(); 
    })
});