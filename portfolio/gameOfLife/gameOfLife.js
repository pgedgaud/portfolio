class GameOfLife {
    constructor(gridWidth, gridHeight) {     
        this.theGrid = [];
        this.gridHeight = gridHeight;
        this.gridWidth =  gridWidth;

        for (var x = 0; x < this.gridWidth; x++) {
            this.theGrid.push([]);

            for (var y = 0; y < this.gridHeight; y++) {
                this.theGrid[x].push(0);     
            }
        }
    }

    generateRandomBoard(){
        for (var x = 0; x < this.theGrid.length; x++) {
            for (var y = 0; y < this.theGrid[x].length; y++) {
                if(Math.random() > 0.5){
                    this.theGrid[x][y] = 0;     
                }
                else {
                    this.theGrid[x][y] = 1;
                }
            }
        }
    }
    
    clearGrid() {
        for (var x = 0; x < this.theGrid.length; x++) {
            for (var y = 0; y < this.theGrid[x].length; y++) {
                this.theGrid[x][y] = 0;
            }
        }
    }
    
    setCellAlive(x, y){
         this.theGrid[x][y] = 1;
    }
    
    isCellAlive(x, y) {
        if (x > this.theGrid.length || x < 0) {
            return false;
        } else {
            if (y > this.theGrid[x].length || y < 0) {
                return false;
            } 
            else {
                return this.theGrid[x][y] === 1;
            }
        }
    }
    
    findNumOfNeighbors(x, y) {
        var num = 0, j, k;
        
        for (j = -1; j < 2; j++) {
            for (k = -1; k < 2; k++) {
                if (!(j === 0 && k === 0) && (x + j) > 0 && (y + k) > 0) {
                    if ( (x + j) < this.theGrid.length && (y + k) < this.theGrid[x + j].length) {
                        if (this.isCellAlive((x + j), (y + k))) {
                            num++;
                        }
                    }
                }
            }
        }
        
        return num;
    }
    
    calculateNextCycle() {      
        var x, y, i, j, newCells = [], num;
        
        for (i = 0; i < this.theGrid.length; i++) {
            newCells[i] = [];
            
            for (j = 0; j < this.theGrid[i].length; j++) {
                newCells[i][j] = 0;
            }
        }
        
        for (x = 0; x < this.theGrid.length; x++) {
            for (y = 0; y < this.theGrid[x].length; y++) {
                
                num = this.findNumOfNeighbors(x, y);
                
                if (this.theGrid[x][y] === 1) {
                    if (num === 2 || num === 3) {
                        newCells[x][y] = 1;
                    }
                } else {
                    if (num === 3) {
                        newCells[x][y] = 1;
                    }
                }
            }
        }
        
        this.theGrid = newCells;
    }

    getCellState(x, y) {
        return 
    }
    
    seedGrid() {
        this.clearGrid();
        this.theGrid[5][4] = 1;
        this.theGrid[6][4] = 1;
        this.theGrid[4][5] = 1;
        this.theGrid[5][5] = 1;
        this.theGrid[5][6] = 1;
    }
}