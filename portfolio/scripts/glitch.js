function setCharAt(str, index, chr) {
    if (index > str.length - 1) {
        return str;
    }

    return str.substr(0, index) + chr + str.substr(index + 1);
}

function glitch(i, text, orgText) {
    if (i > 50) {
        unglitch(i, text, orgText);
        return;
    }

    var replaceableChars = "abcdefghijklm!@#$%^&*(){}[]\\/?|-_+=nopqrstuvwxyz!@#$%^&*(){}[]\\/?|-_+=ABCDEFGHIJKLMNO!@#$%^&*(){}[]\\/?|-_+=PQRSTUVWXYZ1234567890!@#$%^&*(){}[]\\/?|-_+=";
    var indexToReplace = Math.floor(Math.random() * text.length);
    var replacementChar = Math.floor(Math.random() * replaceableChars.length);
    var newText = setCharAt(text, indexToReplace, replaceableChars.charAt(replacementChar));
    //document.getElementById("title").innerText = newText;
    document.title = newText;
    i++;
    setTimeout(function () { glitch(i, newText, orgText) }, 20);
}

function unglitch(i, text, orgText) {
    if (i < 0) {
        //document.getElementById("title").innerText = orgText;
        document.title = "Philip Gedgaud";
        return;
    }

    var indexToReplace = Math.floor(Math.random() * text.length);
    var newText = setCharAt(text, indexToReplace, orgText.charAt(indexToReplace));
    //document.getElementById("title").innerText = newText;
    document.title = newText;
    i--;
    setTimeout(function () { unglitch(i, newText, orgText) }, 20);
}

function startGlitch() {
    var text = document.title;//document.getElementById("title").innerText;
    var i = 0;
    glitch(i, text, text);

    var interval = setInterval(function () {
        var text = document.title;//document.getElementById("title").innerText;
        var i = 0;
        glitch(i, text, text);
    }, 15000);
}
