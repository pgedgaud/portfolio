﻿document.addEventListener('DOMContentLoaded', function () {
    while (document.getElementsByClassName('jsWarning').length > 0) {
        var warning = document.getElementsByClassName('jsWarning')[0];
        warning.parentElement.removeChild(warning);
    }

    var gameOfLife = document.getElementById("gameOfLife");
    var contact = document.getElementById("contact");
    var about = document.getElementById("about");
    gameOfLife.style.display = "none";
    contact.style.display = "none";

    document.getElementById("gameLink").addEventListener("click", function (e) {
        e.preventDefault();
        document.getElementById("gameOfLife").style.display = "";
        document.getElementById("contact").style.display = "none";
        document.getElementById("about").style.display = "none";
    });

    document.getElementById("contactLink").addEventListener("click", function (e) {
        e.preventDefault();
        document.getElementById("gameOfLife").style.display = "none";
        document.getElementById("contact").style.display = "";
        document.getElementById("about").style.display = "none";
    });

    document.getElementById("aboutLink").addEventListener("click", function (e) {
        e.preventDefault();
        document.getElementById("gameOfLife").style.display = "none";
        document.getElementById("contact").style.display = "none";
        document.getElementById("about").style.display = "";
    });

    startGlitch();

});