class Attractor {
    constructor(x, y, mass){
        this.location = new Vector(x, y);
        this.mass = mass;
        this.gravityConstant = 0.4;
    }

    get Location() {
        return this.location;
    }

    attract(m) {
        var force = Vector.sub(this.location, m.location);
        var distance = force.mag();
        distance = constrain(distance, 5.0, 25.0);
        force.normalize();
        var strength = (this.gravityConstant * this.mass * m.mass) / (distance * distance);
        force.mult(strength);
        return force;
    }
}