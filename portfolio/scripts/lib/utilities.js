function constrain (thing, min, max) {
    if(thing < min) {
        return min;
    } else if (thing > max){
        return max; 
    } else {
        return thing;
    }
}