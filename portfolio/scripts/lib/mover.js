class Mover {
    constructor(location, velocity, acceleration, mass){
        this.location = location;
        this.velocity = velocity;
        this.acceleration = acceleration;
        this.mass = mass;
        this.gravityConstant = 1;
    }

    get Location() {
        return this.location;
    }

    applyForce(force) {
        var f = new Vector(force.X, force.Y);
        f.div(this.mass);
        this.acceleration.add(f);
    }

    update() {
        this.velocity.add(this.acceleration);
        this.location.add(this.velocity);
        this.acceleration.mult(0);
    }

    attract(m) {
        var force = Vector.sub(this.location, m.location);
        var distance = force.mag();
        distance = constrain(distance, 5.0, 25.0);
        force.normalize();
        var strength = (this.gravityConstant * this.mass * m.mass) / (distance * distance);
        force.mult(strength);
        return force;
    }
}