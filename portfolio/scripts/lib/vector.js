class Vector {
    constructor(x, y){
        
        this.x = x;
        this.y = y;

        if(x === NaN || x === undefined)
            this.x = 0;

        if(y === NaN || x === undefined)
            this.y = 0
    }

    get X() {
        return this.x;
    }

    get Y() {
        return this.y;
    }

    set X(val) {
        this.x = val;
    }

    set Y(val) {
        this.y = val;
    }

    add(v) {
        this.y += v.Y;
        this.x += v.X;
    }

    div(n) {
        if(typeof n === 'number'){
            this.y /= n;
            this.x /= n;
        }
    }

    mult(n) {
        if(typeof n === 'number') {
            this.y *= n;
            this.x *= n;
        }
    }

    mag(){
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    normalize() {
        var m = this.mag();
        if(m != 0) {
            this.div(m);
        }
    }

    limit(n) {
        if (this.mag() > n) {
            this.normalize();
            this.mult(max);
        }
    }

    static dist(v1, v2){
        return Math.sqrt(Math.pow(v2.x - v1.x, 2) + Math.pow(v2.y - v1.y, 2));
    }

    static sub (v1, v2) {
        return new Vector(v1.X - v2.X, v1.Y - v2.Y);
    }
}